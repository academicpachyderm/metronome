/*
 * To do:
 * Beep when tempo changes
 * Hysterisis to "lock in" tempo
 */

#define OUT_PIN A6  // Output pin. LED for now, tone later

uint16_t msPerMin = 60000; // Milliseconds in a minute. Make this a const?

int onTime = 1;   // ms on/playing tone. This is a constant the same for any bpm. Should be the minimum amount of time needed to hear the tick
int offTime = 490;  // ms off/silent time
int period = 500;   // Period of 1 beat (ms)
int bpm = 120;

void setup() {
  pinMode(OUT_PIN, OUTPUT);
}

void loop() {
  getBpm();
  bpmToMs(bpm);
  digitalWrite(OUT_PIN, 1);
  delay(onTime);
  digitalWrite(OUT_PIN, 0);
  delay(offTime);
}

void bpmToMs(int bpm){  // Oh whoops. I'm overloading bpm. Fix that eventually
  // This function takes in a bpm value from 15(?) to 255 bpm and sets period and offTime accordingly
  // ms / beat = msPerMin / bpm
  period = msPerMin / bpm;
  offTime = period - onTime;
}

void getBpm(){
  int reading = analogRead(A2); // get potentiometer value (0 to 1023)
  //reading >> 2; // reading is now 0 to 255
  int mappedReading = map(reading, 0, 1023, 60, 280);   // Scale the raw reading value to a useful value
  bpm = mappedReading;
}
